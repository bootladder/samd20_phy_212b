##############################################################################
CONFIG = public

##############################################################################
.PHONY: all directory clean size

STACK_PATH = .

CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
OBJCOPY = arm-none-eabi-objcopy
SIZE = arm-none-eabi-size

CFLAGS += -W -Wall --std=gnu99 -O0
CFLAGS += -fdata-sections -ffunction-sections
CFLAGS += -funsigned-char -funsigned-bitfields
CFLAGS += -mcpu=cortex-m0plus -mthumb
CFLAGS += -MD -MP -MT $(CONFIG)/$(*F).o -MF $(CONFIG)/$(@F).d

LDFLAGS += -mcpu=cortex-m0plus -mthumb
#LDFLAGS += -Wl,--gc-sections
#LDFLAGS += -Wl,--script=../linker/atsamd20j18.ld

INCLUDES += \
  -I../samd20_cmsis_headers \
  -I../arm_cmsis_headers

SRCS += \
  halPhy.c \
  phy.c \

DEFINES += \
  -DPHY_AT86RF212B \
  -DHAL_ATSAMD20J18 \
  -DF_CPU=48000000  \
  -DPHY_ENABLE_AES_MODULE

CFLAGS += $(INCLUDES) $(DEFINES)

#Take $(SRCS) ignore directories , change .c to .o , add dest path prefix
#OBJS = $(addprefix $(CONFIG)/, $(notdir %/$(subst .c,.o, $(SRCS))))
OBJS = $(CONFIG)/phy.o $(CONFIG)/halPhy.o 

all: directory $(CONFIG)/libSAMD20_PHY_212B.a \
               size

$(CONFIG)/libSAMD20_PHY_212B.a: $(OBJS)
	@echo LD $@
	@$(AR) -r -o $@ $(OBJS) 
	@cp SAMD20_PHY_212B.h $(CONFIG)
	@cp SAMD20_PHY_212B.h ../lib
	@cp $(CONFIG)/libSAMD20_PHY_212B.a ../lib

$(CONFIG)/halPhy.o:
	@echo CC $@
	@$(CC) $(CFLAGS) halPhy.c -c -o $@ 

$(CONFIG)/phy.o:
	@echo CC $@
	@$(CC) $(CFLAGS) phy.c -c -o $@ 

#%.o:
#	@echo CC $@
#	@$(CC) $(CFLAGS) $(filter %/$(subst .o,.c,$(notdir $@)), $(SRCS)) -c -o $@

directory:
	@mkdir -p $(CONFIG)

size: $(CONFIG)/libSAMD20_PHY_212B.a
	@echo size:
	@$(SIZE) -t $^

clean:
	@echo clean
	@-rm -rf $(CONFIG)

-include $(wildcard $(CONFIG)/*.d)
